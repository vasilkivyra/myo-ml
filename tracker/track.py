from time import sleep
from internals.myo_win import init, Hub, DeviceListener, StreamEmg
import numpy as np
import os
from sklearn.externals import joblib
import matplotlib
import sklearn.metrics as metrics
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib import animation
from drawnow import drawnow, figure

display_size            = 100
window_size             = 45
classify_interval       = 50 # run classification every 20 data points

current_class = 'rectangle'

pca = joblib.load('./models/pca_' + current_class + '.pkl')
clf = joblib.load('./models/classifier_' + current_class + '.pkl')

classes             = ['other gestures', current_class]
currentClassifyLabel    = ''

running_acc_data        = np.empty(shape=(0, 3))
running_gyro_data       = np.empty(shape=(0, 3))
running_ori_data        = np.empty(shape=(0, 4))

classifyCounter         = 0

class Listener(DeviceListener):
    runningClusters = np.empty(shape=0, dtype=int)
    runningData = np.empty(shape=(0, 8))

    def on_connected(self, event):
        event.device.stream_emg(StreamEmg.enabled)

    def on_paired(self, event):
        print("Hello, Myo!")

    def on_unpaired(self, event):
        print("Goodbye, Myo!")

    def on_orientation(self, event):
        global running_ori_data
        running_ori_data = np.append(running_ori_data, np.expand_dims(np.array(list(event.orientation)), axis=0), axis=0)
        running_ori_data = running_ori_data[-1*100:, :]

        global running_gyro_data
        running_gyro_data = np.append(running_gyro_data, np.expand_dims(np.array(list(event.gyroscope)), axis=0), axis=0)
        running_gyro_data = running_gyro_data[-1*100:, :]

        global running_acc_data, classifyCounter, currentClassifyLabel, classes
        running_acc_data = np.append(running_acc_data, np.expand_dims(np.array(list(event.acceleration)), axis=0), axis=0)
        running_acc_data = running_acc_data[-1*100:, :]

        classifyCounter += 1
        if running_ori_data. shape[0] >= window_size and \
           running_acc_data. shape[0] >= window_size and \
           running_gyro_data.shape[0] >= window_size and \
           classifyCounter % classify_interval == 0:

            ori_data            = running_ori_data [-1*window_size:, :]
            acc_data            = running_acc_data [-1*window_size:, :]
            gyro_data           = running_gyro_data[-1*window_size:, :]
            imu_data = np.append(acc_data, gyro_data, axis=1)

            fft                 = np.abs(np.fft.fft(imu_data, axis=0))
            fft                 = fft.reshape(-1)

            reduced_x           = pca.transform(np.expand_dims(fft, axis=0))
            cls                 = clf.predict(reduced_x)
            print(classes[int(cls[0])])
            currentClassifyLabel = classes[int(cls[0])]


def update_line(_lines, _classify_label):
    global running_acc_data, running_gyro_data, currentClassifyLabel
    min_size = min(running_acc_data.shape[0], running_gyro_data.shape[0])
    imu_data = np.append(running_acc_data[:min_size], running_gyro_data[:min_size], axis=1)

    _classify_label.set_text(currentClassifyLabel)
    if currentClassifyLabel == 'wave':
        _classify_label.set_bbox({'facecolor': 'red', 'alpha':0.5, 'pad':10})
    else:
        _classify_label.set_bbox({'facecolor': 'green', 'alpha':0.5, 'pad':10})

    for _i in range(6):
        d1 = np.transpose(imu_data[:, _i])
        dd = np.append(np.expand_dims(np.array(range(imu_data.shape[0])), axis=0), np.expand_dims(d1, axis=0), axis=0)
        _lines[_i].set_data(dd)
    return _lines[0], _lines[1], _lines[2], _lines[3], _lines[4], _lines[5],


if __name__ == '__main__':
    fig = plt.figure()
    plt.rc('font', size=8)
    lines = []
    for i in range(6):
        ax = fig.add_subplot(7, 1, (i+2))
        ax.set_xlim([0, 100])
        if i < 3:
            ax.set_ylim([-2, 2])
        else:
            ax.set_ylim([-100, 100])
        l, = ax.plot([])
        lines.append(l)

    classifyLabel = plt.text(30, 1500, '--', style='italic', fontsize=30, bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})

    plt.ion()
    plt.show()

    init()
    hub = Hub()
    while hub.run(Listener(), 10):
        try:
            update_line(lines, classifyLabel)
            plt.draw()
            plt.pause(1.0 / 30)
        except KeyboardInterrupt:
            print('\nQuit')
        finally:
            hub.stop()  # !! crucial
