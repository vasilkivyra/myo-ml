import numpy as np
import pandas as pd
import os
from matplotlib import pyplot
from sklearn.cluster import KMeans
from sklearn import neighbors, metrics
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
import time
from random import choices
from sklearn.model_selection import LeaveOneOut
from sklearn.model_selection import KFold

window_size         = 45  # 50Hz data -> 0.2 seconds per window
n_training          = 20  # number of trials used for training, the rest for validation
n_components        = 40  # PCA components
imu_dimensions      = 6   # number of dimensions in imu data

classes             = ['relax', 'slide_up', 'slide_down', 'slide_left', 'slide_right', 'circle', 'rectangle']

current_class = 'rectangle'

trainingActivities  = []
testingActivities   = []

def create_models_dir_struct():
    try:
        # Create target Directory
        os.mkdir('./models')
        print("Directory " + './models' + " has been created ")
    except FileExistsError:
        pass

## compute fft features (amplitude of frequencies)
def compute_imu_features(df):
    # grab center 'window_size' data points
    start = int((df.shape[0] - window_size)/2)
    d = df[start:start+window_size, 0:imu_dimensions]
    fft = np.abs(np.fft.fft(d, axis=0))
    fft = fft.reshape(-1)
    return fft

def build_data(_pca, activities):
    training_x = np.empty(shape=(0, n_components))
    training_y = np.empty(shape=(0, n_components))
    for (_x, _y) in activities:
        reduced_x = _pca.transform(np.expand_dims(_x, axis=0))
        training_x = np.append(training_x, reduced_x, axis=0)
        training_y = np.append(training_y, _y)
    return training_x, training_y


## load in the activities trial and put in training and testing set
def load_data():
    train_data  = []
    test_data   = []
    temp_data   = []

    for idx, cls in enumerate(classes):
        file_index = 1
        while True:
            path = './data/' + cls + '/imu/' + str(file_index) + '.txt'
            if not os.path.exists(path):
                break

            is_training_data = file_index <= n_training
            raw_x = np.loadtxt(path, dtype=np.float32, delimiter=',')
            raw_x = raw_x[:, :-4]
            _x = compute_imu_features(raw_x)
            # print (cls, raw_x.shape, x.shape)

            if is_training_data:
                if cls == current_class:
                    train_data.append((_x, 1))
                else:
                    temp_data.append((_x, 0))
            else:
                if cls == current_class:
                    testingActivities.append((_x, 1))
                else:
                    testingActivities.append((_x, 0))

            file_index += 1


    if len(temp_data) > 0:
        train_data.extend(choices(temp_data, k=len(train_data)))

    trainingActivities.extend(train_data)

    print('training activities:', len(trainingActivities),
          'testing activities:', len(testingActivities))


        # plot the first instance of each activity type to visualize the fft features
    # for _i in range(len(classes)):
    #     pyplot.figure(_i)
    #     pyplot.title(classes[_i])
    #     training_activity = trainingActivities[_i * n_training]
    #     pyplot.plot(training_activity[0])

def apply_pca():
    ## apply PCA to reduce X dimensions
    all_training_x = np.empty(shape=(0, window_size * imu_dimensions))
    all_testing_x  = np.empty(shape=(0, window_size * imu_dimensions))

    for (x, y) in trainingActivities:
        all_training_x = np.append(all_training_x, np.expand_dims(x, axis=0), axis=0)

    for (x, y) in testingActivities:
        all_testing_x  = np.append(all_testing_x,  np.expand_dims(x, axis=0), axis=0)

    pca = PCA(n_components=n_components).fit(all_training_x)
    pyplot.plot(pca.explained_variance_ratio_.cumsum())  ## show explained variance

    training_X = pca.transform(all_training_x)
    testing_X = pca.transform(all_testing_x)

    # (training_X, training_Y)  = build_data(pca, trainingActivities)
    # (testing_X , testing_Y)   = build_data(pca, testingActivities)

    kf = KFold(n_splits=5, shuffle=False, random_state=None)
    for train_index, test_index in kf.split(trainingActivities):
        print("Train:", train_index, "Validation:", test_index)
        # X_train, X_test = trainingActivities[train_index], trainingActivities[test_index]
        # y_train, y_test = y[train_index], y[test_index]
        training_data, testing_data = trainingActivities[train_index], trainingActivities[test_index]

    (training_X, training_Y) = build_data(pca, trainingActivities)
    (testing_X, testing_Y) = build_data(pca, testingActivities)

    print (training_X, training_Y)
    print (testing_X,  testing_Y)

    # for i in range(len(classes)):
    #     pyplot.figure(i)
    #     pyplot.title(classes[i])
    #     pyplot.plot(training_X[i * n_training])

    for i in range(len(classes)):
        pyplot.figure(i)
        pyplot.plot(testing_X[i])

    clf = SVC(kernel='linear')

    clf.fit(training_X, training_Y)

    print ('Score: {0:f}'.format(clf.score(training_X, training_Y)))

    print (clf.predict(testing_X))
    print (testing_Y)
    print (np.sum(clf.predict(testing_X) == testing_Y), testing_Y.shape[0])

    print("Confusin Matrix:")
    print (confusion_matrix(testing_Y, clf.predict(testing_X)))

    create_models_dir_struct()

    joblib.dump(clf, './models/classifier_'+current_class+'.pkl')
    joblib.dump(pca, './models/pca_'+current_class+'.pkl')

    pca2 = joblib.load('./models/pca_'+current_class+'.pkl')
    clf2 = joblib.load('./models/classifier_'+current_class+'.pkl')


    start_time = time.time()
    (testingX, testingY) = build_data(pca2, testingActivities)
    print (clf2.predict(testingX))
    end_time = time.time()
    print ("Take: " + str(end_time-start_time))


if __name__ == '__main__':
    load_data()
    apply_pca()
