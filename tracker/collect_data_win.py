from internals.myo_win import init, Hub, DeviceListener, StreamEmg, VibrationType
import os
import numpy as np
import keyboard
import threading
from os import remove
import sys

class MyoDataRecorder(DeviceListener):
    def __init__(self, myo_hub, folder, label, rec_time_sec):
        self.folder = folder
        self.label  = label
        self.create_data_dir()
        self.emg_data_counter = 0
        self.imu_data_counter = 0
        self.acc_data  = np.empty(shape=(0, 4))
        self.gyro_data = np.empty(shape=(0, 3))
        self.ori_data  = np.empty(shape=(0, 4))
        self.emg_data  = np.empty(shape=(0, 9))
        self.act_counter = 1
        self.isRecording = False
        self.isDiscarded = False
        self.isQuit = False
        self.isChangeLabel = False
        self.myo_hub = myo_hub
        self.rec_time_sec = rec_time_sec
        self.update_files_counter()

    def update_files_counter(self):
        emg_folder = self.folder + '/' + self.label + '/imu/'
        listdir_count = len([name for name in os.listdir(emg_folder) if os.path.isfile(os.path.join(emg_folder, name))])
        if listdir_count == 0:
            self.act_counter = 1
        else:
            self.act_counter = listdir_count + 1

        print("\x1b[32;1m[INFO]: The data folder contains " + str(listdir_count) + " recordings\x1b[0m")

    def update_data_dir(self):
        try:
            os.mkdir(self.folder + '/' + self.label)
            print("\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + " has been created \x1b[0m")
        except FileExistsError:
            print("\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + " already exists! \x1b[0m")

        try:
            # Create target Directory
            os.mkdir(self.folder + '/' + self.label + '/' + 'emg')
            print(
                "\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + '/' + 'emg' + " has been created \x1b[0m")
        except FileExistsError:
            print(
                "\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + '/' + 'emg' + " already exists! \x1b[0m")

        try:
            # Create target Directory
            os.mkdir(self.folder + '/' + self.label + '/' + 'imu')
            print(
                "\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + '/' + 'imu' + " has been created \x1b[0m")
        except FileExistsError:
            print(
                "\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + '/' + 'imu' + " already exists! \x1b[0m")

    def create_data_dir(self):
        try:
            # Create target Directory
            os.mkdir(self.folder)
            print("\x1b[32;1m[INFO]: Directory " + self.folder + " has been created \x1b[0m")
        except FileExistsError:
            print("\x1b[33;1m[WARNING]: Directory " + self.folder + " already exists! \x1b[0m")


        try:
            os.mkdir(self.folder + '/' + self.label)
            print("\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + " has been created \x1b[0m")
        except FileExistsError:
            print("\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + " already exists! \x1b[0m")

        try:
            # Create target Directory
            os.mkdir(self.folder + '/' + self.label + '/' + 'emg')
            print("\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + '/' + 'emg' + " has been created \x1b[0m")
        except FileExistsError:
            print("\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + '/' + 'emg' +" already exists! \x1b[0m")


        try:
            # Create target Directory
            os.mkdir(self.folder + '/' + self.label + '/' + 'imu')
            print("\x1b[32;1m[INFO]: Directory " + self.folder + '/' + self.label + '/' + 'imu' + " has been created \x1b[0m")
        except FileExistsError:
            print("\x1b[33;1m[WARNING]: Directory " + self.folder + '/' + self.label + '/' + 'imu' +" already exists! \x1b[0m")

    def on_connected(self, event):
        event.device.stream_emg(True)
        event.device.vibrate(VibrationType.short)

    def on_emg(self, event):
        if not self.isRecording:
          return

        data = np.hstack([event.timestamp, event.emg])
        self.emg_data = np.append(self.emg_data, np.expand_dims(np.array(list(data)), axis=0), axis=0)
        self.emg_data_counter+=1
        print('\r\033[KEMG values: ' + str(self.emg_data_counter) + ', IMU values: ' + str(self.imu_data_counter), end='')
        sys.stdout.flush()

    def on_paired(self, event):
        print("Hello, Myo!\n")

    def on_unpaired(self, event):
        print("Goodbye, Myo!\n")

    def on_orientation(self, event):
        if not self.isRecording:
          return

        self.ori_data = np.append(self.ori_data, np.expand_dims(np.array(list(event.orientation)), axis=0), axis=0)

        gy = event.gyroscope
        self.gyro_data = np.append(self.gyro_data, np.expand_dims(np.array(list(gy)), axis=0), axis=0)

        acc = np.hstack([event.timestamp, np.asarray(list(event.acceleration))])
        self.acc_data = np.append(self.acc_data, np.expand_dims(np.array(list(acc)), axis=0), axis=0)
        self.imu_data_counter+=1

    def save_data(self):

        imu_data = np.append(np.append(self.acc_data, self.gyro_data, axis=1), self.ori_data, axis=1)

        np.savetxt(self.folder + '/' + self.label + '/imu/' + str(self.act_counter) + '.txt', imu_data, fmt='%1.0d,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f,%1.4f')
        np.savetxt(self.folder + '/' + self.label + '/emg/' + str(self.act_counter) + '.txt', self.emg_data, delimiter=',', fmt='%1.0f')

        self.acc_data    = np.empty (shape=(0, 4))
        self.gyro_data   = np.empty (shape=(0, 3))
        self.ori_data    = np.empty (shape=(0, 4))
        self.emg_data    = np.empty (shape=(0, 9))

        print("\x1b[32;1m[INFO]: saved to " + self.folder + '/' + self.label + '/emg/' + str(self.act_counter) + '.txt' + "\x1b[0m")
        print("\x1b[32;1m[INFO]: saved to " + self.folder + '/' + self.label + '/imu/' + str(self.act_counter) + '.txt' + "\x1b[0m")

        self.act_counter += 1



    def stop_recording(self):
        print('\nstop recording: \'' + self.label + '\''+": trial #"+str(self.act_counter))

        print('EMG: ',end='')
        if self.emg_data_counter > 0:
            print(str(self.emg_data_counter) + '/' + '{0}'.format(int(200 * self.rec_time_sec)) + ' samples')
        else:
            print('\x1b[31;1m'+str(self.emg_data_counter) + '/' + '{0}'.format(int(200 * self.rec_time_sec)) + ' samples' + '\x1b[0m')

        print('IMU: ',end='')
        if self.imu_data_counter > 0:
            print(str(self.imu_data_counter) + '/' + '{0}'.format(int(50 * self.rec_time_sec)) + ' samples')
        else:
            print('\x1b[31;1m'+str(self.imu_data_counter) + '/' + '{0}'.format(int(50 * self.rec_time_sec)) + ' samples' + '\x1b[0m')

        self.isRecording = False
        self.save_data()

        imufile_size = os.path.getsize(self.folder + '/' + self.label + '/imu/' + str(self.act_counter-1) + '.txt')
        emgfile_size = os.path.getsize(self.folder + '/' + self.label + '/emg/' + str(self.act_counter-1) + '.txt')

        bad = emgfile_size < 32 * (200 * self.rec_time_sec)
        if bad:
            print('\x1b[33;1m[WARNING]:\x1b[0m EMG expected MIN size: ' + '{0}'.format(int(32 * 200 * self.rec_time_sec)) + ' bytes, current size: ' + '{0}'.format(int(emgfile_size)) +' bytes')

        bad = imufile_size < 92 * (50 * self.rec_time_sec)
        if bad:
            print('\x1b[33;1m[WARNING]:\x1b[0m IMU expected MIN size: ' + '{0}'.format(int(92 * 50 * self.rec_time_sec)) + ' bytes, current size: ' + '{0}'.format(int(imufile_size)) +' bytes')

        if bad:
            if self.emg_data_counter < 175 * self.rec_time_sec or self.imu_data_counter < 40 * self.rec_time_sec:
                print("\x1b[31;1m[CRITICAL]: Consider discarding this recording (" + str(self.act_counter-1) + '.txt) by pressing \'d\'\x1b[0m')
            else:
                print("\x1b[33;1m[WARNING]:\x1b[0m Consider discarding this recording (" + str(self.act_counter-1) + '.txt) by pressing \'d\'')

        self.emg_data_counter = 0
        self.imu_data_counter = 0

    def reset_discarded(self):
        self.isDiscarded = False

    def reset_quit(self):
        self.isQuit = False

    def reset_change_label(self):
        self.isChangeLabel = False

    def key_press(self, key):
        if key.name == 'r':
            if self.isChangeLabel:
                return

            if self.isRecording:
                return
            threading.Timer(self.rec_time_sec, self.stop_recording).start()
            print('\nstart recording: \''+self.label+'\''+": trial #"+str(self.act_counter))
            self.isRecording = True
            self.isDiscarded = False


        elif key.name == 'd':
            if self.isChangeLabel:
                return

            if not self.isDiscarded:
                if self.isRecording:
                    print('\x1b[32;1m\n[INFO]: Cannot remove file while recording.\x1b[0m\n')
                    self.isDiscarded = True
                    threading.Timer(0.5, self.reset_discarded).start()
                    return

                self.isDiscarded = True

                if self.act_counter > 1:
                    self.discard_the_last_recording()
                else:
                    print('\x1b[32;1m\n[INFO]: No recordings left to remove.\x1b[0m')

                threading.Timer(0.5, self.reset_discarded).start()

        elif key.name == 'n':
            if not self.isChangeLabel:
                self.isChangeLabel = True
                lbl = input("\x1b[32;1m \n[INFO]: Enter new data LABEL: ")
                while len(lbl) == 0:
                    print("\x1b[33;1m[WARN]: You have entered empty string!")
                    lbl = input("\x1b[32;1m[INFO]: Enter new data LABEL: ")

                print('\x1b[32;1m\n[INFO]: LABEL has been changed to ' + self.label + '.\x1b[0m\n')
                self.label = lbl
                threading.Timer(0.5, self.reset_change_label).start()
                self.update_data_dir()
                self.update_files_counter()

        elif key.name == 'q':
            if self.isChangeLabel:
                return
            if not self.isQuit:
                if self.isRecording:
                    print('\x1b[32;1m \n[INFO]: Cannot quit while recording.\x1b[0m\n')
                    self.isQuit = True
                    threading.Timer(0.5, self.reset_quit).start()
                    return
                else:
                    self.myo_hub.stop()
                    os._exit(0)

    def discard_the_last_recording(self):
        self.isRecording = False

        prev_file_num = self.act_counter - 1

        if prev_file_num == 0:
            return

        last_imu_file = self.folder + '/' + self.label + '/imu/' + str(prev_file_num) + '.txt'
        last_emg_file = self.folder + '/' + self.label + '/emg/' + str(prev_file_num) + '.txt'

        remove(last_emg_file)
        remove(last_imu_file)

        print('\n\'\x1b[32;1m[INFO]: ' + self.label + '\''+", trial #" + str(prev_file_num) + " has been discarded.\x1b[0m")

        self.act_counter -= 1

    def run(self):
        while self.myo_hub.run(self.on_event, 500):
            try:
                keyboard.on_release(self.key_press)
            except KeyboardInterrupt:
                print('\nQuit')
            finally:
                self.myo_hub.stop()  # !! crucial


TASK_1  = 'tv_turn_on'
TASK_2  = 'tv_next_channel'
TASK_3  = 'ac_temp_down'
TASK_4  = 'audio_volume_down'
TASK_5  = 'lights_bri_down'
TASK_6  = 'tv_sound_mute'
TASK_7  = 'audio_pause'
TASK_8  = 'blinds_close'
TASK_9  = 'video_fast_rewind'
TASK_10 = 'doors_close'

if __name__ == '__main__':
    DATA_FOLDER = './999'
    LABEL       = TASK_2

    init()
    hub = Hub()
    recorder = MyoDataRecorder(hub, DATA_FOLDER, LABEL, 1.5)
    recorder.run()



