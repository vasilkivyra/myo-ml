from internals.myo_linux.myo_utils import MyoRecorder
import os
import numpy as np
import threading

import pygame
from pygame.locals import *

act_counter = 1
isRecording = False

acc_data = np.empty(shape=(0, 3))
gyro_data = np.empty(shape=(0, 3))
ori_data = np.empty(shape=(0, 4))
emg_data = np.empty(shape=(0, 9))


def setup_COM_port():
    os.chmod('/dev/ttyACM0', 777)


def create_data_dir_struct():
    try:
        # Create target Directory
        os.mkdir(DATA_FOLDER)
        print("Directory " + DATA_FOLDER + " has been created ")
    except FileExistsError:
        pass

    try:
        os.mkdir(DATA_FOLDER + '/' + ACTIVITY)
        print("Directory " + DATA_FOLDER + '/' + ACTIVITY + " has been created ")
    except FileExistsError:
        pass

    try:
        # Create target Directory
        os.mkdir(DATA_FOLDER + '/' + ACTIVITY + '/' + 'emg')
        print("Directory " + DATA_FOLDER + '/' + ACTIVITY + '/' + 'emg' + " has been created ")
    except FileExistsError:
        pass

    try:
        # Create target Directory
        os.mkdir(DATA_FOLDER + '/' + ACTIVITY + '/' + 'imu')
        print("Directory " + DATA_FOLDER + '/' + ACTIVITY + '/' + 'imu' + " has been created ")
    except FileExistsError:
        pass


def check_press_key():
    for ev in pygame.event.get():
        if ev.type == QUIT or (ev.type == KEYDOWN and ev.unicode == 'q'):
            raise KeyboardInterrupt()
        elif ev.type == KEYDOWN and ev.unicode == 'r':
            if recorder.is_recording():
                return
            recorder.discard_the_last_recording()
            threading.Timer(1.0, recorder.stop_recording).start()
        elif ev.type == KEYDOWN:
            if ev.key == K_SPACE:
                if recorder.is_recording():
                    return
                recorder.start_recording()
                threading.Timer(1.0, recorder.stop_recording).start()


if __name__ == '__main__':

    DATA_FOLDER = '../data'
    ACTIVITY = 'aaaaaaaaaaaaaaa'

    create_data_dir_struct()
    pygame.init()

    # setup_COM_port()
    recorder = MyoRecorder(data_folder=DATA_FOLDER, data_label=ACTIVITY)
    pygame.display.set_mode((400, 400))

    recorder.connect()
    # setup device
    recorder.sleep_mode(1)
    recorder.set_leds([128, 128, 255], [128, 128, 255])  # purple logo and bar LEDs
    recorder.vibrate(1)

    # main loop
    try:
        while True:
            recorder.run()
            check_press_key()
            pygame.display.flip()
    except Exception as e:
        print(e)
    finally:
        print('\nQuit')
        print("Sleep Mode is turned back ON. Disconnecting...")
        recorder.sleep_mode(0)
        recorder.set_leds([0, 0, 0], [0, 0, 0])  # purple logo and bar LEDs
        recorder.mc_end_collection()
        recorder.disconnect()
        os._exit(0)

    pygame.quit()

