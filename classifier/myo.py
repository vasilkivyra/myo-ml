from __future__ import print_function

from collections import Counter, deque
import sys

import numpy as np
from sklearn import neighbors, metrics

# from internals.myo_win.collector import MyoRaw
from internals.myo_linux.myo import MyoRaw
from classifier.common import *


SUBSAMPLE   = 3
K           = 25
HIST_LEN    = 25


class NNClassifier(object):
    """A wrapper for sklearn's nearest-neighbor classifier that stores
    training data in emg_gest_class, ..., emg_gest_class.dat."""

    def __init__(self, name, filename, columns, fmt, dtype):
        self.name = name
        self.filename = filename
        self.columns = columns
        self.dtype = dtype
        self.fmt = fmt
        for i in range(10):
            with open(self.filename+'_class%d.dat' % i, 'ab') as f: pass
        self.read_data()

    def store_data(self, cls, vals):
        with open(self.filename+'_class%d.dat' % cls, 'ab') as f:
            f.write(pack(str(self.columns) + self.fmt, *vals))

        self.train(np.vstack([self.X, vals]), np.hstack([self.Y, [cls]]))

    def read_data(self):
        X = []
        Y = []
        for i in range(10):
            X.append(np.fromfile(self.filename+'_class%d.dat' % i, dtype=self.dtype).reshape((-1, self.columns)))
            Y.append(i + np.zeros(X[-1].shape[0]))

        self.train(np.vstack(X), np.hstack(Y))

    def train(self, X, Y):
        self.X = X
        self.Y = Y
        if self.X.shape[0] >= K * SUBSAMPLE:
            self.nn = neighbors.KNeighborsClassifier(n_neighbors=K, algorithm='kd_tree')
            self.nn.fit(self.X[::SUBSAMPLE], self.Y[::SUBSAMPLE])
        else:
            self.nn = None

    def nearest(self, d):
        dists = ((self.X - d)**2).sum(1)
        ind = dists.argmin()
        return self.Y[ind]

    def classify(self, d):
        if self.X.shape[0] < K * SUBSAMPLE: return 0
        y_pred = self.nn.predict(d)
        return int(y_pred[0])


class EMGHandler(object):
    def __init__(self, myo_handler):
        self.recording = -1
        self.myo_handler = myo_handler
        self.data = np.zeros((1, 8))

    def __call__(self, emg):
        self.data = np.asarray(emg)
        if self.recording >= 0:
            self.myo_handler.classifiers['emg'].store_data(self.recording, self.data)


class IMUHandler(object):
    def __init__(self, myo_handler):
        self.recording = -1
        self.myo_handler = myo_handler
        self.data = np.zeros((1, 6))

    def __call__(self, quat, gyro, accel):
        self.data = np.hstack([np.asarray(gyro), np.asarray(accel)])
        if self.recording >= 0:
            self.myo_handler.classifiers['imu'].store_data(self.recording, self.data)


class Myo(MyoRaw):
    """Adds higher-level pose classification and handling onto MyoRaw."""

    class History:
        def __init__(self, length):
            self.history = deque([0] * length, length)
            self.history_cnt = Counter(self.history)

    def __init__(self, tty=None, emg=True, imu=False):
        MyoRaw.__init__(self, tty)
        self.classifiers = {}
        self.histories   = {}
        self.handlers    = {}

        if emg:
            self.classifiers['emg'] = NNClassifier('emg', 'emg_gest', 8, 'h', np.ushort)
            self.add_emg_handler(self.emg_handler)
            self.histories  ['emg'] = Myo.History(HIST_LEN)

            handler = EMGHandler(self)
            self.add_emg_handler(handler)
            self.handlers ['emg'] = handler

        if imu:
            self.classifiers['imu'] = NNClassifier('imu', 'imu_gest', 6, 'i', np.int)
            self.add_imu_handler(self.imu_handler)
            self.histories  ['imu'] = Myo.History(round(HIST_LEN))

            handler = IMUHandler(self)
            self.add_imu_handler(handler)
            self.handlers['imu'] = handler

        self.last_pose = None
        self.pose_handlers = []

    def emg_handler(self, emg):
        cols = self.classifiers['emg'].columns
        y = self.classifiers['emg'].classify(np.asarray(emg).reshape(1, cols))
        self.histories['emg'].history_cnt[self.histories['emg'].history[0]] -= 1
        self.histories['emg'].history_cnt[y] += 1
        self.histories['emg'].history.append(y)

        r, n = self.histories['emg'].history_cnt.most_common(1)[0]
        if self.last_pose is None or (n > self.histories['emg'].history_cnt[self.last_pose] + 5 and n > HIST_LEN / 2):
            self.on_raw_pose(r)
            self.last_pose = r

    def imu_handler(self, quat, gyro, accel):
        cols = self.classifiers['imu'].columns
        y = self.classifiers['imu'].classify(np.hstack([gyro, accel]).reshape(1, cols))
        self.histories['imu'].history_cnt[self.histories['imu'].history[0]] -= 1
        self.histories['imu'].history_cnt[y] += 1
        self.histories['imu'].history.append(y)

        r, n = self.histories['imu'].history_cnt.most_common(1)[0]
        if self.last_pose is None or (n > self.histories['imu'].history_cnt[self.last_pose] + 5 and n > HIST_LEN / 2):
            self.on_raw_pose(r)
            self.last_pose = r

    def add_raw_pose_handler(self, h):
        self.pose_handlers.append(h)

    def on_raw_pose(self, pose):
        for h in self.pose_handlers:
            h(pose)
