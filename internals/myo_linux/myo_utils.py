import os
import os.path
from os import remove

import numpy as np

from internals.myo_linux.myo import MyoRaw


class MyoRecorder(MyoRaw):
    def __init__(self, data_folder='data/', data_label='dafault'):
        MyoRaw.__init__(self, None)
        self.data_folder = data_folder
        self.recording = False
        self.data_label_counter = 1
        self.data_label = data_label

        self.acc_data = np.empty(shape=(0, 3))
        self.gyro_data = np.empty(shape=(0, 3))
        self.ori_data = np.empty(shape=(0, 4))
        self.emg_data = np.empty(shape=(0, 8))

        self.add_emg_handler(self.emg_handler)
        self.add_imu_handler(self.imu_handler)

        self.last_pose = None
        self.pose_handlers = []

        emg_folder = self.data_folder + '/' + self.data_label + '/imu/'
        listdir_count = len([name for name in os.listdir(emg_folder) if os.path.isfile(os.path.join(emg_folder, name))])
        if listdir_count == 0:
            self.data_label_counter = 1
        else:
            self.data_label_counter = listdir_count + 1

    def is_recording(self):
        return self.recording

    def start_recording(self):
        self.recording = True
        print('start recording: \''+self.data_label+'\''+": trial #"+str(self.data_label_counter))

    def stop_recording(self):
        self.recording = False
        print('stop recording: \''+self.data_label+'\'')
        self._save_data()

    def discard_the_last_recording(self):
        self.recording = False
        last_imu_file = self.data_folder + '/' + self.data_label + '/imu/' + str(self.data_label_counter - 1) + '.txt'
        last_emg_file = self.data_folder + '/' + self.data_label + '/emg/' + str(self.data_label_counter - 1) + '.txt'
        remove(last_emg_file)
        remove(last_imu_file)

        print('\''+self.data_label+'\''+", trial #"+str(self.data_label_counter - 1) + " is discarded.")

        if self.data_label_counter > 1:
            self.data_label_counter -= 2


    def emg_handler(self, emg):
        if not self.recording:
            return

        data = np.hstack(emg)
        self.emg_data = np.append(self.emg_data, np.expand_dims(np.array(list(data)), axis=0), axis=0)
        pass

    def imu_handler(self, quat, gyro, accel):
        if not self.recording:
            return

        self.ori_data = np.append(self.ori_data, np.expand_dims(np.array(list(quat)), axis=0), axis=0)
        self.gyro_data = np.append(self.gyro_data, np.expand_dims(np.array(list(gyro)), axis=0), axis=0)
        self.acc_data = np.append(self.acc_data, np.expand_dims(np.array(list(accel)), axis=0), axis=0)
        pass

    def set_label(self, data_label):
        self.data_label = data_label

    def _save_data(self):
        imu_data = np.append(np.append(self.acc_data, self.gyro_data, axis=1), self.ori_data, axis=1)

        np.savetxt(self.data_folder + '/' + self.data_label + '/imu/' + str(self.data_label_counter) + '.txt',
                   imu_data, delimiter=',', fmt='%1.0f')

        np.savetxt(self.data_folder + '/' + self.data_label + '/emg/' + str(self.data_label_counter) + '.txt',
                   self.emg_data, delimiter=',', fmt='%1.0f')

        self.acc_data = np.empty(shape=(0, 3))
        self.gyro_data = np.empty(shape=(0, 3))
        self.ori_data = np.empty(shape=(0, 4))
        self.emg_data = np.empty(shape=(0, 8))
        self.data_label_counter += 1

