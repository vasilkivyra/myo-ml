from pathlib import Path
from easydict import EasyDict
# IMU_WINDOW_SIZE = 45  # 50Hz data -> 0.2 seconds per window for imu data
# EMG_WINDOW_SIZE = 175
# IMU_DIMENSIONS = 6  # number of dimensions in imu data
# EMG_DIMENSIONS = 8  # number of dimensions in emg data

_PARAMETERS = {}
_EXPORT_DIR = Path('../exported_models')
_N_TRAINING = 20  # number of trials used for training, the rest for validation
_N_TOTAL = 30
_N_COMPONENTS = 40  # PCA components
PARTICIPANTS_OA = ['oa_y_p_1', 'oa_y_p_2', 'oa_y_p_3', 'oa_y_p_4', 'oa_y_p_5', 'oa_y_p_6',
                   'oa_y_p_7', 'oa_y_p_8', 'oa_y_p_9', 'oa_y_p_10', 'oa_y_p_11', 'oa_y_p_12']
PARTICIPANTS_YA = ['ya_a_p_1', 'ya_a_p_2', 'ya_a_p_3', 'ya_y_p_4', 'ya_y_p_5', 'ya_y_p_6',
                   'ya_y_p_7', 'ya_y_p_8', 'ya_y_p_9', 'ya_y_p_10', 'ya_y_p_11', 'ya_y_p_12']

PARTICIPANTS = PARTICIPANTS_OA + PARTICIPANTS_YA
_CURRENT_PARAMETERS = None


def get_data_parameters(name=None):
    global _CURRENT_PARAMETERS
    if name is None:
        if _CURRENT_PARAMETERS is None:
            raise Exception('First call to this function should pass `name`')
        return _CURRENT_PARAMETERS
    _CURRENT_PARAMETERS = EasyDict(_PARAMETERS[name])
    return _CURRENT_PARAMETERS


def add_data(name, data_path, classes, imu_window_size, imu_dimensions,
             emg_window_size, emg_dimensions, participant_type="n/a"):
    temp = {}
    temp['name'] = name
    temp['classes'] = classes
    temp['data_path'] = data_path
    temp['imu_window_size'] = imu_window_size
    temp['imu_dimensions'] = imu_dimensions
    temp['emg_window_size'] = emg_window_size
    temp['emg_dimensions'] = emg_dimensions
    temp['export_dir'] = _EXPORT_DIR
    temp['n_training'] = _N_TRAINING
    temp['n_total'] = _N_TOTAL
    temp['n_components'] = _N_COMPONENTS
    temp['participant_type'] = participant_type
    _PARAMETERS[name] = temp


def add_participant_data(name, data_path, classes_list=None):
    if classes_list is None:
        classes_list = ['ac_temp_down', 'audio_pause', 'audio_volume_down', 'blinds_close', 'doors_close',
                        'lights_bri_down', 'tv_next_channel', 'tv_sound_mute', 'tv_turn_on', 'video_fast_rewind']
    add_data(name,
             data_path,
             classes_list,
             70, 10, 274, 8,
             "oa" if "oa" in name else "ya" if "ya" in name else "n/a")


add_data('data1', Path('../data/data1'),
         ['circle', 'rectangle', 'relax', 'slide_down', 'slide_left', 'slide_right', 'slide_up'],
         45, 6, 190, 8)

add_data('data2', Path('../data/data2'),
         ['blinds_close', 'close_doors', 'fast_rewind', 'light_down', 'mute_tv', 'next_channel',
          'pause_audio', 'rest', 'temp_down', 'turn_on_tv', 'volume_down'],
         45, 6, 190, 8)  # TODO: Dont remeber the values, recheck!

add_data('data3', Path('../data/data3'),
         ['claws', 'fist', 'fuck_you', 'ok', 'palm_fingers_double_click',
          'rest', 'thumb_swipe_left', 'wave_left_come-here', 'wave_right_get-out', 'yyyeeeeeeees'],
         45, 6, 45, 8)

add_data('data4', Path('../data/data4'),
         ['ac_down', 'close_blinds', 'close_door', 'fast_rewind', 'light_down', 'music_down',
          'music_stop', 'next_channel_tv', 'rest', 'tv_mute', 'tv_turn_on'],
         45, 6, 175, 8)

add_data('data5', Path('../data/data5'),
         ['tv_next_channel', 'turn_on_tv', 'rest', 'pause_audio', 'mute_tv', 'music_down', 'light_down',
          'fast_rewind', 'close_doors', 'blinds_close', 'ac_down'],
         45, 6, 190, 8)

add_data('yurii_p_1', Path('../data/yurii_participant_1'),
         ['ac_temp_down', 'audio_pause', 'audio_volume_down', 'blinds_close', 'doors_close',
          'lights_bri_down', 'tv_next_channel', 'tv_sound_mute', 'tv_turn_on', 'video_fast_rewind'],
         74, 10, 296, 8)


# ############################# YA ################################
add_participant_data('ya_a_p_1', Path('../data/YA/ALI_participant_A1'))
add_participant_data('ya_a_p_2', Path('../data/YA/ALI_participant_A2'))
add_participant_data('ya_a_p_3', Path('../data/YA/ALI_participant_A3'))
add_participant_data('ya_y_p_4', Path('../data/YA/Yurii_participant_1'))
add_participant_data('ya_y_p_5', Path('../data/YA/Yurii_participant_2'))
add_participant_data('ya_y_p_6', Path('../data/YA/Yurii_participant_3'))
add_participant_data('ya_y_p_7', Path('../data/YA/Yurii_participant_4'))
add_participant_data('ya_y_p_8', Path('../data/YA/Yurii_participant.5'))
add_participant_data('ya_y_p_9', Path('../data/YA/Yurii_participant.6'))
add_participant_data('ya_y_p_10', Path('../data/YA/Yurii_participant.7'))
add_participant_data('ya_y_p_11', Path('../data/YA/Yurii_participant.8'))
add_participant_data('ya_y_p_12', Path('../data/YA/Yurii_participant.9'))


# ############################# OA ################################
add_participant_data('oa_y_p_1', Path('../data/OA/Yurii_participant.OA.1'))
add_participant_data('oa_y_p_2', Path('../data/OA/Yurii_participant.OA.2'))
add_participant_data('oa_y_p_3', Path('../data/OA/Yurii_participant.OA.3'))
add_participant_data('oa_y_p_4', Path('../data/OA/Yurii_participant.OA.4'))
add_participant_data('oa_y_p_5', Path('../data/OA/Yurii_participant.OA.5'))
add_participant_data('oa_y_p_6', Path('../data/OA/Yurii_participant.OA.6'))
add_participant_data('oa_y_p_7', Path('../data/OA/Yurii_participant.OA.7'))
add_participant_data('oa_y_p_8', Path('../data/OA/Yurii_participant.OA.8'))
add_participant_data('oa_y_p_9', Path('../data/OA/Yurii_participant.OA.9'))
add_participant_data('oa_y_p_10', Path('../data/OA/Yurii_participant.OA.10'))
add_participant_data('oa_y_p_11', Path('../data/OA/Yurii_participant.OA.11'))
add_participant_data('oa_y_p_12', Path('../data/OA/Yurii_participant.OA.12'))
