import numpy as np
from pathlib import Path
import shutil
from mlpipeline import Versions, MetricContainer, log
from mlpipeline.base import ExperimentABC, DataLoaderCallableWrapper
import mlflow
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from matplotlib import pyplot
import time

from data_utils import (DataLoader,
                        create_models_dir_struct,
                        build_data,
                        log_conf_metrix)
from parameters import get_data_parameters, PARTICIPANTS


class Experiment(ExperimentABC):
    def setup_model(self, ):
        self.clf = SVC(kernel='linear', probability=True)
        # self.clf = neighbors.KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree')
        self.pca = PCA(n_components=self.current_version.parameters.n_components) if self.current_version.use_pca else None

    def pre_execution_hook(self, **kwargs):
        self.metrics = ['score_scikit', 'score_calced', 'recall', 'precision']
        self._gened_files = 0
        mlflow.log_param("encoding", self.dataloader.encoding)

    def _pca_fit(self, data):
        all_x = np.empty(shape=(0, self.current_version.input_size))
        for (x, y) in data:
            all_x = np.append(all_x, np.expand_dims(x, axis=0), axis=0)
        self.pca.fit(all_x)
        pyplot.plot(self.pca.explained_variance_ratio_.cumsum())  # show explained variance

    def train_loop(self, input_fn, **kwargs):
        if self.current_version.use_pca:
            self._pca_fit(input_fn)
        training_X, training_Y = build_data(input_fn,
                                            self.current_version.parameters.n_components,
                                            self.pca)
        self.clf.fit(training_X, training_Y)

    def evaluate_loop(self, input_fn, **kwargs):
        testing_X, testing_Y = build_data(input_fn,
                                          self.current_version.parameters.n_components,
                                          self.pca)
        metricContainer = MetricContainer(self.metrics)
        metricContainer.score_scikit.update(self.clf.score(testing_X, testing_Y).item(), 1)
        metricContainer.score_calced.update(np.sum(
            self.clf.predict(testing_X) == testing_Y).item() / len(testing_Y), 1)
        conf_matrix = confusion_matrix(testing_Y, self.clf.predict(testing_X))
        # Not calculating precision and recall when multiclass models is trained
        if testing_Y.max() <= 1:
            metricContainer.recall.update(conf_matrix[1, 1]/(conf_matrix[1, 0] + conf_matrix[1, 1]), 1)
            metricContainer.precision.update(conf_matrix[1, 1]/(conf_matrix[0, 1] + conf_matrix[1, 1]), 1)
        log_conf_metrix(conf_matrix, self._gened_files, self.experiment_dir, self.dataloader.encoding)
        self._gened_files += 1
        return metricContainer

    def export_model(self, **kwargs):
        create_models_dir_struct(self.current_version.parameters.export_dir)
        self._export_model(self.current_version.parameters.export_dir)

    def _export_model(self, export_dir):
        current_class = self.current_version.name
        self.log(f"Saving models in: {export_dir}")
        joblib.dump(self.clf, export_dir / ('classifier_'+current_class+'.pkl'))
        if self.current_version.use_pca:
            joblib.dump(self.pca, export_dir / ('pca_'+current_class+'.pkl'))

        self.clf = joblib.load(export_dir / ('classifier_'+current_class+'.pkl'))
        try:
            self.pca = joblib.load(export_dir / ('pca_'+current_class+'.pkl'))
        except FileNotFoundError:
            self.pca = None

    def post_execution_hook(self, **kwargs):
        self.log("Executing export model")
        self._export_model(Path(self.experiment_dir))

        self.log("Running sanity check")
        start_time = time.time()
        (testingX, testingY) = build_data(self.dataloader.get_test_input(),
                                          self.current_version.parameters.n_components,
                                          self.pca)
        self.log(self.clf.predict(testingX))
        end_time = time.time()
        self.log("Time taken: " + str(end_time-start_time))

    def clean_experiment_dir(self, model_dir):
        self.log("CLEANING MODEL DIR")
        shutil.rmtree(model_dir, ignore_errors=True)


# if __name__ == "__main__":
v = Versions()


def add_version(name, cls, parameters, data_type, input_size, use_pca):
    v.add_version(name + f'_{_p.name}',
                  dataloader=DataLoaderCallableWrapper(DataLoader,
                                                       parameters=_p,
                                                       data_type=data_type,
                                                       current_class=cls),
                  parameters=_p,
                  input_size=input_size,
                  use_pca=use_pca,
                  data_type=data_type,
                  data_used=_p.name,
                  participant_type=_p.participant_type,
                  used_class=cls if cls is not None else _p.classes)  # Just to log the value in mlflow


# log("Running tests")
# problem_params = []
for param in PARTICIPANTS:
    _p = get_data_parameters(param)
    # try:
    #     log(param)
    #     DataLoader(parameters=_p, data_type="all")
    #     # DataLoader(parameters=_p, data_type="emg")
    # except Exception:
    #     problem_params.append(param)
    #     continue
    for cls in _p.classes:
        add_version(f'{cls}_with_imu', cls, _p, 'imu', _p.imu_window_size * _p.imu_dimensions, False)

    for cls in _p.classes:
        add_version(f'{cls}_with_emg', cls, _p, 'emg', _p.emg_window_size * _p.emg_dimensions, False)

    for cls in _p.classes:
        input_size = _p.emg_window_size * _p.emg_dimensions + _p.imu_window_size * _p.imu_dimensions
        add_version(f'{cls}_with_all', cls, _p, 'all', input_size, False)

    # for cls in _p.classes:
    #     add_version(f'{cls}_with_imu_pca', cls, _p, 'imu', _p.imu_window_size * _p.imu_dimensions, True)

    # for cls in _p.classes:
    #     add_version(f'{cls}_with_emg_pca', cls, _p, 'emg', _p.emg_window_size * _p.emg_dimensions, True)

    # for cls in _p.classes:
    #     input_size = _p.emg_window_size * _p.emg_dimensions + _p.imu_window_size * _p.imu_dimensions
    #     add_version(f'{cls}_with_all_pca', cls, _p, 'all', input_size, True)

    # add_version(f'ALL_CLASSES_with_all_pca', None, _p, 'all',
    #             _p.emg_window_size * _p.emg_dimensions + _p.imu_window_size * _p.imu_dimensions, True)

    # add_version(f'ALL_CLASSES_with_imu_pca', None, _p, 'imu',
    #             _p.imu_window_size * _p.imu_dimensions, True)

    # add_version(f'ALL_CLASSES_with_emg_pca', None, _p, 'emg',
    #             _p.emg_window_size * _p.emg_dimensions, True)

    add_version(f'ALL_CLASSES_with_all', None, _p, 'all',
                _p.emg_window_size * _p.emg_dimensions + _p.imu_window_size * _p.imu_dimensions, False)

    add_version(f'ALL_CLASSES_with_imu', None, _p, 'imu',
                _p.imu_window_size * _p.imu_dimensions, False)

    add_version(f'ALL_CLASSES_with_emg', None, _p, 'emg',
                _p.emg_window_size * _p.emg_dimensions, False)

# log(f"End of tests")
# if len(problem_params) > 0:
#     log(f"Problems in: {problem_params}")
# v.filter_versions(whitelist_versions=[ver for ver in v.get_version_names() if 'oa_y_p_2' in ver])
# v.filter_versions(whitelist_versions=["ALL_CLASSES_with_emg_ya_y_p_11"])
EXPERIMENT = Experiment(v, True)
