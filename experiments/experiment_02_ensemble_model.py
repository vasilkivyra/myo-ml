import numpy as np
import mlflow
import shutil
from pathlib import Path
from easydict import EasyDict
from mlpipeline.base import ExperimentABC, DataLoaderCallableWrapper
from mlpipeline import Versions, MetricContainer
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix

from parameters import get_data_parameters, PARTICIPANTS
from data_utils import build_data, DataLoader, log_conf_metrix


class Model:
    def __init__(self, classes, models_path, use_pca, parameters, data_type='imu'):
        self.models = {}
        self.parameters = parameters
        for idx, current_class in enumerate(classes):
            o = EasyDict()
            current_class = f'{current_class}_with_{data_type}'
            if use_pca:
                current_class += '_pca'
            current_class += f'_{parameters.name}'
            o.clf = joblib.load(models_path /
                                ('experiment_01_classification_models-' + current_class) /
                                ('classifier_' + current_class + '.pkl'))
            try:
                o.pca = joblib.load(models_path /
                                    ('experiment_01_classification_models-' + current_class) /
                                    ('pca_' + current_class + '.pkl'))
            except FileNotFoundError:
                o.pca = None
            self.models[idx] = o

    def predict(self, data, return_scores=False, start=None, end=None):
        results = {}
        for idx, model in self.models.items():
            x, y = build_data(data,
                              self.parameters.n_components,
                              model.pca,
                              start,
                              end)
            results[idx] = model.clf.predict_proba(x)[:, 1]
        results = np.concatenate([r.reshape(-1, 1) for r in results.values()], axis=1)
        if return_scores:
            return results
        results = np.argmax(results, axis=1)
        return results


class ModelCombined:
    def __init__(self, classes, models_path, use_pca, parameters, *args, **kwargs):
        self.emg_model = Model(classes, models_path, use_pca, parameters, 'emg')
        self.imu_model = Model(classes, models_path, use_pca, parameters, 'imu')
        self.parameters = parameters

    def predict(self, data, **kwargs):
        mid_point = self.parameters.emg_dimensions * self.parameters.emg_window_size
        emg_results = self.emg_model.predict(data, True, end=mid_point)
        imu_results = self.imu_model.predict(data, True, start=mid_point)
        results = emg_results + imu_results
        results = np.argmax(results, axis=1)
        return results


class Experiment(ExperimentABC):
    def setup_model(self, ):
        self.model = self.current_version.model(self.current_version.classes,
                                                Path(self.current_version.models_path),
                                                self.current_version.use_pca,
                                                self.current_version.parameters,
                                                self.current_version.data_type)

    def pre_execution_hook(self, **kwargs):
        self.metrics = ['score_scikit', 'score_calced']
        self._gened_files = 0
        mlflow.log_param("encoding", self.dataloader.encoding)

    def clean_experiment_dir(self, model_dir):
        self.log("CLEANING MODEL DIR")
        shutil.rmtree(model_dir, ignore_errors=True)

    # def train_loop(self, input_fn, **kwargs):
    #     training_X, training_Y = input_fn
    #     self.clf.fit(training_X, training_Y)

    def evaluate_loop(self, input_fn, **kwargs):
        metricContainer = MetricContainer(self.metrics)
        results = self.model.predict(input_fn)
        _, testing_Y = build_data(input_fn, self.current_version.parameters.n_components)
        metricContainer.score_calced.update(np.sum(
            results == testing_Y).item() / len(testing_Y), 1)
        conf_matrix = confusion_matrix(testing_Y, results)
        self.log("Index mapping: {}".format({idx: cls
                                             for idx, cls in enumerate(self.current_version.classes)}),
                 log_to_file=True)
        log_conf_metrix(conf_matrix, self._gened_files, self.experiment_dir, self.dataloader.encoding)
        self._gened_files += 1
        return metricContainer


v = Versions(DataLoaderCallableWrapper(DataLoader))


def add_version(name, parameters,  model, data_type, use_pca):
    v.add_version(name + f'_{parameters.name}',
                  dataloader=DataLoaderCallableWrapper(DataLoader,
                                                       parameters=parameters,
                                                       data_type=data_type),
                  model=model,
                  parameters=parameters,
                  classes=parameters.classes,
                  data_type=data_type,
                  use_pca=use_pca,
                  data_used=parameters.name,
                  file_path=parameters.data_path,
                  participant_type=parameters.participant_type,
                  models_path="outputs/experiment_ckpts/")


for param in PARTICIPANTS:
    _p = get_data_parameters(param)

    add_version('ensemble_imu', _p, Model, 'imu', False)
    add_version('ensemble_emg', _p, Model, 'emg', False)
    add_version('ensemble_all', _p, Model, 'all', False)
    add_version('ensemble_joined', _p, ModelCombined, 'all', False)

    # add_version('ensemble_imu_pca', _p, Model, 'imu', True)
    # add_version('ensemble_emg_pca', _p, Model, 'emg', True)
    # add_version('ensemble_all_pca', _p, Model, 'all', True)
    # add_version('ensemble_joined_pca', _p, ModelCombined, 'all', True)

# v.filter_versions(whitelist_versions=['ensemble_all'])
EXPERIMENT = Experiment(v, False)
